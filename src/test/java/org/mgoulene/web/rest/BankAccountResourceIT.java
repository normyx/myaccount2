package org.mgoulene.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mgoulene.IntegrationTest;
import org.mgoulene.domain.BankAccount;
import org.mgoulene.domain.User;
import org.mgoulene.repository.BankAccountRepository;
import org.mgoulene.repository.search.BankAccountSearchRepository;
import org.mgoulene.service.criteria.BankAccountCriteria;
import org.mgoulene.service.dto.BankAccountDTO;
import org.mgoulene.service.mapper.BankAccountMapper;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BankAccountResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class BankAccountResourceIT {

    private static final String DEFAULT_ACCOUNT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_BANK = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_BANK = "BBBBBBBBBB";

    private static final Float DEFAULT_INITIAL_AMOUNT = 1F;
    private static final Float UPDATED_INITIAL_AMOUNT = 2F;
    private static final Float SMALLER_INITIAL_AMOUNT = 1F - 1F;

    private static final Boolean DEFAULT_ARCHIVED = false;
    private static final Boolean UPDATED_ARCHIVED = true;

    private static final String DEFAULT_SHORT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SHORT_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/bank-accounts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/bank-accounts";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankAccountMapper bankAccountMapper;

    /**
     * This repository is mocked in the org.mgoulene.repository.search test package.
     *
     * @see org.mgoulene.repository.search.BankAccountSearchRepositoryMockConfiguration
     */
    @Autowired
    private BankAccountSearchRepository mockBankAccountSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBankAccountMockMvc;

    private BankAccount bankAccount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankAccount createEntity(EntityManager em) {
        BankAccount bankAccount = new BankAccount()
            .accountName(DEFAULT_ACCOUNT_NAME)
            .accountBank(DEFAULT_ACCOUNT_BANK)
            .initialAmount(DEFAULT_INITIAL_AMOUNT)
            .archived(DEFAULT_ARCHIVED)
            .shortName(DEFAULT_SHORT_NAME);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        bankAccount.setAccount(user);
        return bankAccount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankAccount createUpdatedEntity(EntityManager em) {
        BankAccount bankAccount = new BankAccount()
            .accountName(UPDATED_ACCOUNT_NAME)
            .accountBank(UPDATED_ACCOUNT_BANK)
            .initialAmount(UPDATED_INITIAL_AMOUNT)
            .archived(UPDATED_ARCHIVED)
            .shortName(UPDATED_SHORT_NAME);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        bankAccount.setAccount(user);
        return bankAccount;
    }

    @BeforeEach
    public void initTest() {
        bankAccount = createEntity(em);
    }

    @Test
    @Transactional
    void createBankAccount() throws Exception {
        int databaseSizeBeforeCreate = bankAccountRepository.findAll().size();
        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);
        restBankAccountMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isCreated());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeCreate + 1);
        BankAccount testBankAccount = bankAccountList.get(bankAccountList.size() - 1);
        assertThat(testBankAccount.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
        assertThat(testBankAccount.getAccountBank()).isEqualTo(DEFAULT_ACCOUNT_BANK);
        assertThat(testBankAccount.getInitialAmount()).isEqualTo(DEFAULT_INITIAL_AMOUNT);
        assertThat(testBankAccount.getArchived()).isEqualTo(DEFAULT_ARCHIVED);
        assertThat(testBankAccount.getShortName()).isEqualTo(DEFAULT_SHORT_NAME);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(1)).save(testBankAccount);
    }

    @Test
    @Transactional
    void createBankAccountWithExistingId() throws Exception {
        // Create the BankAccount with an existing ID
        bankAccount.setId(1L);
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        int databaseSizeBeforeCreate = bankAccountRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBankAccountMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeCreate);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(0)).save(bankAccount);
    }

    @Test
    @Transactional
    void checkAccountNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setAccountName(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAccountBankIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setAccountBank(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkInitialAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setInitialAmount(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkArchivedIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setArchived(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllBankAccounts() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList
        restBankAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME)))
            .andExpect(jsonPath("$.[*].accountBank").value(hasItem(DEFAULT_ACCOUNT_BANK)))
            .andExpect(jsonPath("$.[*].initialAmount").value(hasItem(DEFAULT_INITIAL_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].archived").value(hasItem(DEFAULT_ARCHIVED.booleanValue())))
            .andExpect(jsonPath("$.[*].shortName").value(hasItem(DEFAULT_SHORT_NAME)));
    }

    @Test
    @Transactional
    void getBankAccount() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get the bankAccount
        restBankAccountMockMvc
            .perform(get(ENTITY_API_URL_ID, bankAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bankAccount.getId().intValue()))
            .andExpect(jsonPath("$.accountName").value(DEFAULT_ACCOUNT_NAME))
            .andExpect(jsonPath("$.accountBank").value(DEFAULT_ACCOUNT_BANK))
            .andExpect(jsonPath("$.initialAmount").value(DEFAULT_INITIAL_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.archived").value(DEFAULT_ARCHIVED.booleanValue()))
            .andExpect(jsonPath("$.shortName").value(DEFAULT_SHORT_NAME));
    }

    @Test
    @Transactional
    void getBankAccountsByIdFiltering() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        Long id = bankAccount.getId();

        defaultBankAccountShouldBeFound("id.equals=" + id);
        defaultBankAccountShouldNotBeFound("id.notEquals=" + id);

        defaultBankAccountShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBankAccountShouldNotBeFound("id.greaterThan=" + id);

        defaultBankAccountShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBankAccountShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountNameIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountName equals to DEFAULT_ACCOUNT_NAME
        defaultBankAccountShouldBeFound("accountName.equals=" + DEFAULT_ACCOUNT_NAME);

        // Get all the bankAccountList where accountName equals to UPDATED_ACCOUNT_NAME
        defaultBankAccountShouldNotBeFound("accountName.equals=" + UPDATED_ACCOUNT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountName not equals to DEFAULT_ACCOUNT_NAME
        defaultBankAccountShouldNotBeFound("accountName.notEquals=" + DEFAULT_ACCOUNT_NAME);

        // Get all the bankAccountList where accountName not equals to UPDATED_ACCOUNT_NAME
        defaultBankAccountShouldBeFound("accountName.notEquals=" + UPDATED_ACCOUNT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountNameIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountName in DEFAULT_ACCOUNT_NAME or UPDATED_ACCOUNT_NAME
        defaultBankAccountShouldBeFound("accountName.in=" + DEFAULT_ACCOUNT_NAME + "," + UPDATED_ACCOUNT_NAME);

        // Get all the bankAccountList where accountName equals to UPDATED_ACCOUNT_NAME
        defaultBankAccountShouldNotBeFound("accountName.in=" + UPDATED_ACCOUNT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountName is not null
        defaultBankAccountShouldBeFound("accountName.specified=true");

        // Get all the bankAccountList where accountName is null
        defaultBankAccountShouldNotBeFound("accountName.specified=false");
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountNameContainsSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountName contains DEFAULT_ACCOUNT_NAME
        defaultBankAccountShouldBeFound("accountName.contains=" + DEFAULT_ACCOUNT_NAME);

        // Get all the bankAccountList where accountName contains UPDATED_ACCOUNT_NAME
        defaultBankAccountShouldNotBeFound("accountName.contains=" + UPDATED_ACCOUNT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountNameNotContainsSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountName does not contain DEFAULT_ACCOUNT_NAME
        defaultBankAccountShouldNotBeFound("accountName.doesNotContain=" + DEFAULT_ACCOUNT_NAME);

        // Get all the bankAccountList where accountName does not contain UPDATED_ACCOUNT_NAME
        defaultBankAccountShouldBeFound("accountName.doesNotContain=" + UPDATED_ACCOUNT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountBankIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBank equals to DEFAULT_ACCOUNT_BANK
        defaultBankAccountShouldBeFound("accountBank.equals=" + DEFAULT_ACCOUNT_BANK);

        // Get all the bankAccountList where accountBank equals to UPDATED_ACCOUNT_BANK
        defaultBankAccountShouldNotBeFound("accountBank.equals=" + UPDATED_ACCOUNT_BANK);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountBankIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBank not equals to DEFAULT_ACCOUNT_BANK
        defaultBankAccountShouldNotBeFound("accountBank.notEquals=" + DEFAULT_ACCOUNT_BANK);

        // Get all the bankAccountList where accountBank not equals to UPDATED_ACCOUNT_BANK
        defaultBankAccountShouldBeFound("accountBank.notEquals=" + UPDATED_ACCOUNT_BANK);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountBankIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBank in DEFAULT_ACCOUNT_BANK or UPDATED_ACCOUNT_BANK
        defaultBankAccountShouldBeFound("accountBank.in=" + DEFAULT_ACCOUNT_BANK + "," + UPDATED_ACCOUNT_BANK);

        // Get all the bankAccountList where accountBank equals to UPDATED_ACCOUNT_BANK
        defaultBankAccountShouldNotBeFound("accountBank.in=" + UPDATED_ACCOUNT_BANK);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountBankIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBank is not null
        defaultBankAccountShouldBeFound("accountBank.specified=true");

        // Get all the bankAccountList where accountBank is null
        defaultBankAccountShouldNotBeFound("accountBank.specified=false");
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountBankContainsSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBank contains DEFAULT_ACCOUNT_BANK
        defaultBankAccountShouldBeFound("accountBank.contains=" + DEFAULT_ACCOUNT_BANK);

        // Get all the bankAccountList where accountBank contains UPDATED_ACCOUNT_BANK
        defaultBankAccountShouldNotBeFound("accountBank.contains=" + UPDATED_ACCOUNT_BANK);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountBankNotContainsSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBank does not contain DEFAULT_ACCOUNT_BANK
        defaultBankAccountShouldNotBeFound("accountBank.doesNotContain=" + DEFAULT_ACCOUNT_BANK);

        // Get all the bankAccountList where accountBank does not contain UPDATED_ACCOUNT_BANK
        defaultBankAccountShouldBeFound("accountBank.doesNotContain=" + UPDATED_ACCOUNT_BANK);
    }

    @Test
    @Transactional
    void getAllBankAccountsByInitialAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where initialAmount equals to DEFAULT_INITIAL_AMOUNT
        defaultBankAccountShouldBeFound("initialAmount.equals=" + DEFAULT_INITIAL_AMOUNT);

        // Get all the bankAccountList where initialAmount equals to UPDATED_INITIAL_AMOUNT
        defaultBankAccountShouldNotBeFound("initialAmount.equals=" + UPDATED_INITIAL_AMOUNT);
    }

    @Test
    @Transactional
    void getAllBankAccountsByInitialAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where initialAmount not equals to DEFAULT_INITIAL_AMOUNT
        defaultBankAccountShouldNotBeFound("initialAmount.notEquals=" + DEFAULT_INITIAL_AMOUNT);

        // Get all the bankAccountList where initialAmount not equals to UPDATED_INITIAL_AMOUNT
        defaultBankAccountShouldBeFound("initialAmount.notEquals=" + UPDATED_INITIAL_AMOUNT);
    }

    @Test
    @Transactional
    void getAllBankAccountsByInitialAmountIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where initialAmount in DEFAULT_INITIAL_AMOUNT or UPDATED_INITIAL_AMOUNT
        defaultBankAccountShouldBeFound("initialAmount.in=" + DEFAULT_INITIAL_AMOUNT + "," + UPDATED_INITIAL_AMOUNT);

        // Get all the bankAccountList where initialAmount equals to UPDATED_INITIAL_AMOUNT
        defaultBankAccountShouldNotBeFound("initialAmount.in=" + UPDATED_INITIAL_AMOUNT);
    }

    @Test
    @Transactional
    void getAllBankAccountsByInitialAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where initialAmount is not null
        defaultBankAccountShouldBeFound("initialAmount.specified=true");

        // Get all the bankAccountList where initialAmount is null
        defaultBankAccountShouldNotBeFound("initialAmount.specified=false");
    }

    @Test
    @Transactional
    void getAllBankAccountsByInitialAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where initialAmount is greater than or equal to DEFAULT_INITIAL_AMOUNT
        defaultBankAccountShouldBeFound("initialAmount.greaterThanOrEqual=" + DEFAULT_INITIAL_AMOUNT);

        // Get all the bankAccountList where initialAmount is greater than or equal to UPDATED_INITIAL_AMOUNT
        defaultBankAccountShouldNotBeFound("initialAmount.greaterThanOrEqual=" + UPDATED_INITIAL_AMOUNT);
    }

    @Test
    @Transactional
    void getAllBankAccountsByInitialAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where initialAmount is less than or equal to DEFAULT_INITIAL_AMOUNT
        defaultBankAccountShouldBeFound("initialAmount.lessThanOrEqual=" + DEFAULT_INITIAL_AMOUNT);

        // Get all the bankAccountList where initialAmount is less than or equal to SMALLER_INITIAL_AMOUNT
        defaultBankAccountShouldNotBeFound("initialAmount.lessThanOrEqual=" + SMALLER_INITIAL_AMOUNT);
    }

    @Test
    @Transactional
    void getAllBankAccountsByInitialAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where initialAmount is less than DEFAULT_INITIAL_AMOUNT
        defaultBankAccountShouldNotBeFound("initialAmount.lessThan=" + DEFAULT_INITIAL_AMOUNT);

        // Get all the bankAccountList where initialAmount is less than UPDATED_INITIAL_AMOUNT
        defaultBankAccountShouldBeFound("initialAmount.lessThan=" + UPDATED_INITIAL_AMOUNT);
    }

    @Test
    @Transactional
    void getAllBankAccountsByInitialAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where initialAmount is greater than DEFAULT_INITIAL_AMOUNT
        defaultBankAccountShouldNotBeFound("initialAmount.greaterThan=" + DEFAULT_INITIAL_AMOUNT);

        // Get all the bankAccountList where initialAmount is greater than SMALLER_INITIAL_AMOUNT
        defaultBankAccountShouldBeFound("initialAmount.greaterThan=" + SMALLER_INITIAL_AMOUNT);
    }

    @Test
    @Transactional
    void getAllBankAccountsByArchivedIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where archived equals to DEFAULT_ARCHIVED
        defaultBankAccountShouldBeFound("archived.equals=" + DEFAULT_ARCHIVED);

        // Get all the bankAccountList where archived equals to UPDATED_ARCHIVED
        defaultBankAccountShouldNotBeFound("archived.equals=" + UPDATED_ARCHIVED);
    }

    @Test
    @Transactional
    void getAllBankAccountsByArchivedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where archived not equals to DEFAULT_ARCHIVED
        defaultBankAccountShouldNotBeFound("archived.notEquals=" + DEFAULT_ARCHIVED);

        // Get all the bankAccountList where archived not equals to UPDATED_ARCHIVED
        defaultBankAccountShouldBeFound("archived.notEquals=" + UPDATED_ARCHIVED);
    }

    @Test
    @Transactional
    void getAllBankAccountsByArchivedIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where archived in DEFAULT_ARCHIVED or UPDATED_ARCHIVED
        defaultBankAccountShouldBeFound("archived.in=" + DEFAULT_ARCHIVED + "," + UPDATED_ARCHIVED);

        // Get all the bankAccountList where archived equals to UPDATED_ARCHIVED
        defaultBankAccountShouldNotBeFound("archived.in=" + UPDATED_ARCHIVED);
    }

    @Test
    @Transactional
    void getAllBankAccountsByArchivedIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where archived is not null
        defaultBankAccountShouldBeFound("archived.specified=true");

        // Get all the bankAccountList where archived is null
        defaultBankAccountShouldNotBeFound("archived.specified=false");
    }

    @Test
    @Transactional
    void getAllBankAccountsByShortNameIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where shortName equals to DEFAULT_SHORT_NAME
        defaultBankAccountShouldBeFound("shortName.equals=" + DEFAULT_SHORT_NAME);

        // Get all the bankAccountList where shortName equals to UPDATED_SHORT_NAME
        defaultBankAccountShouldNotBeFound("shortName.equals=" + UPDATED_SHORT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByShortNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where shortName not equals to DEFAULT_SHORT_NAME
        defaultBankAccountShouldNotBeFound("shortName.notEquals=" + DEFAULT_SHORT_NAME);

        // Get all the bankAccountList where shortName not equals to UPDATED_SHORT_NAME
        defaultBankAccountShouldBeFound("shortName.notEquals=" + UPDATED_SHORT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByShortNameIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where shortName in DEFAULT_SHORT_NAME or UPDATED_SHORT_NAME
        defaultBankAccountShouldBeFound("shortName.in=" + DEFAULT_SHORT_NAME + "," + UPDATED_SHORT_NAME);

        // Get all the bankAccountList where shortName equals to UPDATED_SHORT_NAME
        defaultBankAccountShouldNotBeFound("shortName.in=" + UPDATED_SHORT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByShortNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where shortName is not null
        defaultBankAccountShouldBeFound("shortName.specified=true");

        // Get all the bankAccountList where shortName is null
        defaultBankAccountShouldNotBeFound("shortName.specified=false");
    }

    @Test
    @Transactional
    void getAllBankAccountsByShortNameContainsSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where shortName contains DEFAULT_SHORT_NAME
        defaultBankAccountShouldBeFound("shortName.contains=" + DEFAULT_SHORT_NAME);

        // Get all the bankAccountList where shortName contains UPDATED_SHORT_NAME
        defaultBankAccountShouldNotBeFound("shortName.contains=" + UPDATED_SHORT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByShortNameNotContainsSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where shortName does not contain DEFAULT_SHORT_NAME
        defaultBankAccountShouldNotBeFound("shortName.doesNotContain=" + DEFAULT_SHORT_NAME);

        // Get all the bankAccountList where shortName does not contain UPDATED_SHORT_NAME
        defaultBankAccountShouldBeFound("shortName.doesNotContain=" + UPDATED_SHORT_NAME);
    }

    @Test
    @Transactional
    void getAllBankAccountsByAccountIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);
        User account;
        if (TestUtil.findAll(em, User.class).isEmpty()) {
            account = UserResourceIT.createEntity(em);
            em.persist(account);
            em.flush();
        } else {
            account = TestUtil.findAll(em, User.class).get(0);
        }
        em.persist(account);
        em.flush();
        bankAccount.setAccount(account);
        bankAccountRepository.saveAndFlush(bankAccount);
        Long accountId = account.getId();

        // Get all the bankAccountList where account equals to accountId
        defaultBankAccountShouldBeFound("accountId.equals=" + accountId);

        // Get all the bankAccountList where account equals to (accountId + 1)
        defaultBankAccountShouldNotBeFound("accountId.equals=" + (accountId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBankAccountShouldBeFound(String filter) throws Exception {
        restBankAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME)))
            .andExpect(jsonPath("$.[*].accountBank").value(hasItem(DEFAULT_ACCOUNT_BANK)))
            .andExpect(jsonPath("$.[*].initialAmount").value(hasItem(DEFAULT_INITIAL_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].archived").value(hasItem(DEFAULT_ARCHIVED.booleanValue())))
            .andExpect(jsonPath("$.[*].shortName").value(hasItem(DEFAULT_SHORT_NAME)));

        // Check, that the count call also returns 1
        restBankAccountMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBankAccountShouldNotBeFound(String filter) throws Exception {
        restBankAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBankAccountMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingBankAccount() throws Exception {
        // Get the bankAccount
        restBankAccountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewBankAccount() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();

        // Update the bankAccount
        BankAccount updatedBankAccount = bankAccountRepository.findById(bankAccount.getId()).get();
        // Disconnect from session so that the updates on updatedBankAccount are not directly saved in db
        em.detach(updatedBankAccount);
        updatedBankAccount
            .accountName(UPDATED_ACCOUNT_NAME)
            .accountBank(UPDATED_ACCOUNT_BANK)
            .initialAmount(UPDATED_INITIAL_AMOUNT)
            .archived(UPDATED_ARCHIVED)
            .shortName(UPDATED_SHORT_NAME);
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(updatedBankAccount);

        restBankAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, bankAccountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isOk());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);
        BankAccount testBankAccount = bankAccountList.get(bankAccountList.size() - 1);
        assertThat(testBankAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testBankAccount.getAccountBank()).isEqualTo(UPDATED_ACCOUNT_BANK);
        assertThat(testBankAccount.getInitialAmount()).isEqualTo(UPDATED_INITIAL_AMOUNT);
        assertThat(testBankAccount.getArchived()).isEqualTo(UPDATED_ARCHIVED);
        assertThat(testBankAccount.getShortName()).isEqualTo(UPDATED_SHORT_NAME);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository).save(testBankAccount);
    }

    @Test
    @Transactional
    void putNonExistingBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();
        bankAccount.setId(count.incrementAndGet());

        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBankAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, bankAccountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(0)).save(bankAccount);
    }

    @Test
    @Transactional
    void putWithIdMismatchBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();
        bankAccount.setId(count.incrementAndGet());

        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBankAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(0)).save(bankAccount);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();
        bankAccount.setId(count.incrementAndGet());

        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBankAccountMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(0)).save(bankAccount);
    }

    @Test
    @Transactional
    void partialUpdateBankAccountWithPatch() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();

        // Update the bankAccount using partial update
        BankAccount partialUpdatedBankAccount = new BankAccount();
        partialUpdatedBankAccount.setId(bankAccount.getId());

        partialUpdatedBankAccount.accountName(UPDATED_ACCOUNT_NAME).accountBank(UPDATED_ACCOUNT_BANK).archived(UPDATED_ARCHIVED);

        restBankAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBankAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBankAccount))
            )
            .andExpect(status().isOk());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);
        BankAccount testBankAccount = bankAccountList.get(bankAccountList.size() - 1);
        assertThat(testBankAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testBankAccount.getAccountBank()).isEqualTo(UPDATED_ACCOUNT_BANK);
        assertThat(testBankAccount.getInitialAmount()).isEqualTo(DEFAULT_INITIAL_AMOUNT);
        assertThat(testBankAccount.getArchived()).isEqualTo(UPDATED_ARCHIVED);
        assertThat(testBankAccount.getShortName()).isEqualTo(DEFAULT_SHORT_NAME);
    }

    @Test
    @Transactional
    void fullUpdateBankAccountWithPatch() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();

        // Update the bankAccount using partial update
        BankAccount partialUpdatedBankAccount = new BankAccount();
        partialUpdatedBankAccount.setId(bankAccount.getId());

        partialUpdatedBankAccount
            .accountName(UPDATED_ACCOUNT_NAME)
            .accountBank(UPDATED_ACCOUNT_BANK)
            .initialAmount(UPDATED_INITIAL_AMOUNT)
            .archived(UPDATED_ARCHIVED)
            .shortName(UPDATED_SHORT_NAME);

        restBankAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBankAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBankAccount))
            )
            .andExpect(status().isOk());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);
        BankAccount testBankAccount = bankAccountList.get(bankAccountList.size() - 1);
        assertThat(testBankAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testBankAccount.getAccountBank()).isEqualTo(UPDATED_ACCOUNT_BANK);
        assertThat(testBankAccount.getInitialAmount()).isEqualTo(UPDATED_INITIAL_AMOUNT);
        assertThat(testBankAccount.getArchived()).isEqualTo(UPDATED_ARCHIVED);
        assertThat(testBankAccount.getShortName()).isEqualTo(UPDATED_SHORT_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();
        bankAccount.setId(count.incrementAndGet());

        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBankAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, bankAccountDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(0)).save(bankAccount);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();
        bankAccount.setId(count.incrementAndGet());

        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBankAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(0)).save(bankAccount);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();
        bankAccount.setId(count.incrementAndGet());

        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBankAccountMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(bankAccountDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(0)).save(bankAccount);
    }

    @Test
    @Transactional
    void deleteBankAccount() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        int databaseSizeBeforeDelete = bankAccountRepository.findAll().size();

        // Delete the bankAccount
        restBankAccountMockMvc
            .perform(delete(ENTITY_API_URL_ID, bankAccount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the BankAccount in Elasticsearch
        verify(mockBankAccountSearchRepository, times(1)).deleteById(bankAccount.getId());
    }

    @Test
    @Transactional
    void searchBankAccount() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);
        when(mockBankAccountSearchRepository.search("id:" + bankAccount.getId())).thenReturn(Stream.of(bankAccount));

        // Search the bankAccount
        restBankAccountMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + bankAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME)))
            .andExpect(jsonPath("$.[*].accountBank").value(hasItem(DEFAULT_ACCOUNT_BANK)))
            .andExpect(jsonPath("$.[*].initialAmount").value(hasItem(DEFAULT_INITIAL_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].archived").value(hasItem(DEFAULT_ARCHIVED.booleanValue())))
            .andExpect(jsonPath("$.[*].shortName").value(hasItem(DEFAULT_SHORT_NAME)));
    }
}
