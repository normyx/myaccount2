import { ICategory } from 'app/entities/category/category.model';

export interface ISubCategory {
  id?: number;
  subCategoryName?: string;
  category?: ICategory | null;
}

export class SubCategory implements ISubCategory {
  constructor(public id?: number, public subCategoryName?: string, public category?: ICategory | null) {}
}

export function getSubCategoryIdentifier(subCategory: ISubCategory): number | undefined {
  return subCategory.id;
}
