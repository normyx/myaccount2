import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISubCategory } from '../sub-category.model';
import { SubCategoryService } from '../service/sub-category.service';
import { SubCategoryDeleteDialogComponent } from '../delete/sub-category-delete-dialog.component';

@Component({
  selector: 'jhi-sub-category',
  templateUrl: './sub-category.component.html',
})
export class SubCategoryComponent implements OnInit {
  subCategories?: ISubCategory[];
  isLoading = false;
  currentSearch: string;

  constructor(
    protected subCategoryService: SubCategoryService,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch = this.activatedRoute.snapshot.queryParams['search'] ?? '';
  }

  loadAll(): void {
    this.isLoading = true;
    if (this.currentSearch) {
      this.subCategoryService
        .search({
          query: this.currentSearch,
        })
        .subscribe({
          next: (res: HttpResponse<ISubCategory[]>) => {
            this.isLoading = false;
            this.subCategories = res.body ?? [];
          },
          error: () => {
            this.isLoading = false;
          },
        });
      return;
    }

    this.subCategoryService.query().subscribe({
      next: (res: HttpResponse<ISubCategory[]>) => {
        this.isLoading = false;
        this.subCategories = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ISubCategory): number {
    return item.id!;
  }

  delete(subCategory: ISubCategory): void {
    const modalRef = this.modalService.open(SubCategoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.subCategory = subCategory;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
