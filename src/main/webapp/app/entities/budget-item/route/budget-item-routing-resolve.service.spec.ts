import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IBudgetItem, BudgetItem } from '../budget-item.model';
import { BudgetItemService } from '../service/budget-item.service';

import { BudgetItemRoutingResolveService } from './budget-item-routing-resolve.service';

describe('BudgetItem routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: BudgetItemRoutingResolveService;
  let service: BudgetItemService;
  let resultBudgetItem: IBudgetItem | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(BudgetItemRoutingResolveService);
    service = TestBed.inject(BudgetItemService);
    resultBudgetItem = undefined;
  });

  describe('resolve', () => {
    it('should return IBudgetItem returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultBudgetItem = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultBudgetItem).toEqual({ id: 123 });
    });

    it('should return new IBudgetItem if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultBudgetItem = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultBudgetItem).toEqual(new BudgetItem());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as BudgetItem })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultBudgetItem = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultBudgetItem).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
