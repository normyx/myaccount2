import { IBudgetItemPeriod } from 'app/entities/budget-item-period/budget-item-period.model';
import { ICategory } from 'app/entities/category/category.model';
import { IUser } from 'app/entities/user/user.model';

export interface IBudgetItem {
  id?: number;
  name?: string;
  order?: number;
  budgetItemPeriods?: IBudgetItemPeriod[] | null;
  category?: ICategory | null;
  account?: IUser | null;
}

export class BudgetItem implements IBudgetItem {
  constructor(
    public id?: number,
    public name?: string,
    public order?: number,
    public budgetItemPeriods?: IBudgetItemPeriod[] | null,
    public category?: ICategory | null,
    public account?: IUser | null
  ) {}
}

export function getBudgetItemIdentifier(budgetItem: IBudgetItem): number | undefined {
  return budgetItem.id;
}
