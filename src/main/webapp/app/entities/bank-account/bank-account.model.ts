import { IUser } from 'app/entities/user/user.model';

export interface IBankAccount {
  id?: number;
  accountName?: string;
  accountBank?: string;
  initialAmount?: number;
  archived?: boolean;
  shortName?: string | null;
  account?: IUser;
}

export class BankAccount implements IBankAccount {
  constructor(
    public id?: number,
    public accountName?: string,
    public accountBank?: string,
    public initialAmount?: number,
    public archived?: boolean,
    public shortName?: string | null,
    public account?: IUser
  ) {
    this.archived = this.archived ?? false;
  }
}

export function getBankAccountIdentifier(bankAccount: IBankAccount): number | undefined {
  return bankAccount.id;
}
