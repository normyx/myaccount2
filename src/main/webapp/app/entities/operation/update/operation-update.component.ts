import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IOperation, Operation } from '../operation.model';
import { OperationService } from '../service/operation.service';
import { ISubCategory } from 'app/entities/sub-category/sub-category.model';
import { SubCategoryService } from 'app/entities/sub-category/service/sub-category.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IBankAccount } from 'app/entities/bank-account/bank-account.model';
import { BankAccountService } from 'app/entities/bank-account/service/bank-account.service';

@Component({
  selector: 'jhi-operation-update',
  templateUrl: './operation-update.component.html',
})
export class OperationUpdateComponent implements OnInit {
  isSaving = false;

  subCategoriesSharedCollection: ISubCategory[] = [];
  usersSharedCollection: IUser[] = [];
  bankAccountsSharedCollection: IBankAccount[] = [];

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.maxLength(400)]],
    date: [null, [Validators.required]],
    amount: [null, [Validators.required]],
    note: [null, [Validators.maxLength(400)]],
    checkNumber: [null, [Validators.maxLength(20)]],
    isUpToDate: [null, [Validators.required]],
    deletingHardLock: [],
    subCategory: [],
    account: [],
    bankAccount: [null, Validators.required],
  });

  constructor(
    protected operationService: OperationService,
    protected subCategoryService: SubCategoryService,
    protected userService: UserService,
    protected bankAccountService: BankAccountService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ operation }) => {
      this.updateForm(operation);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const operation = this.createFromForm();
    if (operation.id !== undefined) {
      this.subscribeToSaveResponse(this.operationService.update(operation));
    } else {
      this.subscribeToSaveResponse(this.operationService.create(operation));
    }
  }

  trackSubCategoryById(index: number, item: ISubCategory): number {
    return item.id!;
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  trackBankAccountById(index: number, item: IBankAccount): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOperation>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(operation: IOperation): void {
    this.editForm.patchValue({
      id: operation.id,
      label: operation.label,
      date: operation.date,
      amount: operation.amount,
      note: operation.note,
      checkNumber: operation.checkNumber,
      isUpToDate: operation.isUpToDate,
      deletingHardLock: operation.deletingHardLock,
      subCategory: operation.subCategory,
      account: operation.account,
      bankAccount: operation.bankAccount,
    });

    this.subCategoriesSharedCollection = this.subCategoryService.addSubCategoryToCollectionIfMissing(
      this.subCategoriesSharedCollection,
      operation.subCategory
    );
    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, operation.account);
    this.bankAccountsSharedCollection = this.bankAccountService.addBankAccountToCollectionIfMissing(
      this.bankAccountsSharedCollection,
      operation.bankAccount
    );
  }

  protected loadRelationshipsOptions(): void {
    this.subCategoryService
      .query()
      .pipe(map((res: HttpResponse<ISubCategory[]>) => res.body ?? []))
      .pipe(
        map((subCategories: ISubCategory[]) =>
          this.subCategoryService.addSubCategoryToCollectionIfMissing(subCategories, this.editForm.get('subCategory')!.value)
        )
      )
      .subscribe((subCategories: ISubCategory[]) => (this.subCategoriesSharedCollection = subCategories));

    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('account')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));

    this.bankAccountService
      .query()
      .pipe(map((res: HttpResponse<IBankAccount[]>) => res.body ?? []))
      .pipe(
        map((bankAccounts: IBankAccount[]) =>
          this.bankAccountService.addBankAccountToCollectionIfMissing(bankAccounts, this.editForm.get('bankAccount')!.value)
        )
      )
      .subscribe((bankAccounts: IBankAccount[]) => (this.bankAccountsSharedCollection = bankAccounts));
  }

  protected createFromForm(): IOperation {
    return {
      ...new Operation(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      date: this.editForm.get(['date'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      note: this.editForm.get(['note'])!.value,
      checkNumber: this.editForm.get(['checkNumber'])!.value,
      isUpToDate: this.editForm.get(['isUpToDate'])!.value,
      deletingHardLock: this.editForm.get(['deletingHardLock'])!.value,
      subCategory: this.editForm.get(['subCategory'])!.value,
      account: this.editForm.get(['account'])!.value,
      bankAccount: this.editForm.get(['bankAccount'])!.value,
    };
  }
}
