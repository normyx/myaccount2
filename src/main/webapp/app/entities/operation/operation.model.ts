import dayjs from 'dayjs/esm';
import { ISubCategory } from 'app/entities/sub-category/sub-category.model';
import { IUser } from 'app/entities/user/user.model';
import { IBankAccount } from 'app/entities/bank-account/bank-account.model';
import { IBudgetItemPeriod } from 'app/entities/budget-item-period/budget-item-period.model';

export interface IOperation {
  id?: number;
  label?: string;
  date?: dayjs.Dayjs;
  amount?: number;
  note?: string | null;
  checkNumber?: string | null;
  isUpToDate?: boolean;
  deletingHardLock?: boolean | null;
  subCategory?: ISubCategory | null;
  account?: IUser | null;
  bankAccount?: IBankAccount;
  budgetItem?: IBudgetItemPeriod | null;
}

export class Operation implements IOperation {
  constructor(
    public id?: number,
    public label?: string,
    public date?: dayjs.Dayjs,
    public amount?: number,
    public note?: string | null,
    public checkNumber?: string | null,
    public isUpToDate?: boolean,
    public deletingHardLock?: boolean | null,
    public subCategory?: ISubCategory | null,
    public account?: IUser | null,
    public bankAccount?: IBankAccount,
    public budgetItem?: IBudgetItemPeriod | null
  ) {
    this.isUpToDate = this.isUpToDate ?? false;
    this.deletingHardLock = this.deletingHardLock ?? false;
  }
}

export function getOperationIdentifier(operation: IOperation): number | undefined {
  return operation.id;
}
