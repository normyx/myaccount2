import { ISubCategory } from 'app/entities/sub-category/sub-category.model';
import { CategoryType } from 'app/entities/enumerations/category-type.model';

export interface ICategory {
  id?: number;
  categoryName?: string;
  categoryType?: CategoryType;
  subCategories?: ISubCategory[] | null;
}

export class Category implements ICategory {
  constructor(
    public id?: number,
    public categoryName?: string,
    public categoryType?: CategoryType,
    public subCategories?: ISubCategory[] | null
  ) {}
}

export function getCategoryIdentifier(category: ICategory): number | undefined {
  return category.id;
}
