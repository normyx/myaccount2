import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'bank-account',
        data: { pageTitle: 'myaccountApp.bankAccount.home.title' },
        loadChildren: () => import('./bank-account/bank-account.module').then(m => m.BankAccountModule),
      },
      {
        path: 'budget-item',
        data: { pageTitle: 'myaccountApp.budgetItem.home.title' },
        loadChildren: () => import('./budget-item/budget-item.module').then(m => m.BudgetItemModule),
      },
      {
        path: 'budget-item-period',
        data: { pageTitle: 'myaccountApp.budgetItemPeriod.home.title' },
        loadChildren: () => import('./budget-item-period/budget-item-period.module').then(m => m.BudgetItemPeriodModule),
      },
      {
        path: 'category',
        data: { pageTitle: 'myaccountApp.category.home.title' },
        loadChildren: () => import('./category/category.module').then(m => m.CategoryModule),
      },
      {
        path: 'operation',
        data: { pageTitle: 'myaccountApp.operation.home.title' },
        loadChildren: () => import('./operation/operation.module').then(m => m.OperationModule),
      },
      {
        path: 'sub-category',
        data: { pageTitle: 'myaccountApp.subCategory.home.title' },
        loadChildren: () => import('./sub-category/sub-category.module').then(m => m.SubCategoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
