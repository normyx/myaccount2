import dayjs from 'dayjs/esm';
import { IOperation } from 'app/entities/operation/operation.model';
import { IBudgetItem } from 'app/entities/budget-item/budget-item.model';

export interface IBudgetItemPeriod {
  id?: number;
  date?: dayjs.Dayjs | null;
  month?: dayjs.Dayjs;
  amount?: number;
  isSmoothed?: boolean | null;
  isRecurrent?: boolean | null;
  operation?: IOperation | null;
  budgetItem?: IBudgetItem | null;
}

export class BudgetItemPeriod implements IBudgetItemPeriod {
  constructor(
    public id?: number,
    public date?: dayjs.Dayjs | null,
    public month?: dayjs.Dayjs,
    public amount?: number,
    public isSmoothed?: boolean | null,
    public isRecurrent?: boolean | null,
    public operation?: IOperation | null,
    public budgetItem?: IBudgetItem | null
  ) {
    this.isSmoothed = this.isSmoothed ?? false;
    this.isRecurrent = this.isRecurrent ?? false;
  }
}

export function getBudgetItemPeriodIdentifier(budgetItemPeriod: IBudgetItemPeriod): number | undefined {
  return budgetItemPeriod.id;
}
