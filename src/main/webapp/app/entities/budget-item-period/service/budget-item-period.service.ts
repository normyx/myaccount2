import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { SearchWithPagination } from 'app/core/request/request.model';
import { IBudgetItemPeriod, getBudgetItemPeriodIdentifier } from '../budget-item-period.model';

export type EntityResponseType = HttpResponse<IBudgetItemPeriod>;
export type EntityArrayResponseType = HttpResponse<IBudgetItemPeriod[]>;

@Injectable({ providedIn: 'root' })
export class BudgetItemPeriodService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/budget-item-periods');
  protected resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/_search/budget-item-periods');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(budgetItemPeriod: IBudgetItemPeriod): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(budgetItemPeriod);
    return this.http
      .post<IBudgetItemPeriod>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(budgetItemPeriod: IBudgetItemPeriod): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(budgetItemPeriod);
    return this.http
      .put<IBudgetItemPeriod>(`${this.resourceUrl}/${getBudgetItemPeriodIdentifier(budgetItemPeriod) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(budgetItemPeriod: IBudgetItemPeriod): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(budgetItemPeriod);
    return this.http
      .patch<IBudgetItemPeriod>(`${this.resourceUrl}/${getBudgetItemPeriodIdentifier(budgetItemPeriod) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBudgetItemPeriod>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBudgetItemPeriod[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBudgetItemPeriod[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  addBudgetItemPeriodToCollectionIfMissing(
    budgetItemPeriodCollection: IBudgetItemPeriod[],
    ...budgetItemPeriodsToCheck: (IBudgetItemPeriod | null | undefined)[]
  ): IBudgetItemPeriod[] {
    const budgetItemPeriods: IBudgetItemPeriod[] = budgetItemPeriodsToCheck.filter(isPresent);
    if (budgetItemPeriods.length > 0) {
      const budgetItemPeriodCollectionIdentifiers = budgetItemPeriodCollection.map(
        budgetItemPeriodItem => getBudgetItemPeriodIdentifier(budgetItemPeriodItem)!
      );
      const budgetItemPeriodsToAdd = budgetItemPeriods.filter(budgetItemPeriodItem => {
        const budgetItemPeriodIdentifier = getBudgetItemPeriodIdentifier(budgetItemPeriodItem);
        if (budgetItemPeriodIdentifier == null || budgetItemPeriodCollectionIdentifiers.includes(budgetItemPeriodIdentifier)) {
          return false;
        }
        budgetItemPeriodCollectionIdentifiers.push(budgetItemPeriodIdentifier);
        return true;
      });
      return [...budgetItemPeriodsToAdd, ...budgetItemPeriodCollection];
    }
    return budgetItemPeriodCollection;
  }

  protected convertDateFromClient(budgetItemPeriod: IBudgetItemPeriod): IBudgetItemPeriod {
    return Object.assign({}, budgetItemPeriod, {
      date: budgetItemPeriod.date?.isValid() ? budgetItemPeriod.date.format(DATE_FORMAT) : undefined,
      month: budgetItemPeriod.month?.isValid() ? budgetItemPeriod.month.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
      res.body.month = res.body.month ? dayjs(res.body.month) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((budgetItemPeriod: IBudgetItemPeriod) => {
        budgetItemPeriod.date = budgetItemPeriod.date ? dayjs(budgetItemPeriod.date) : undefined;
        budgetItemPeriod.month = budgetItemPeriod.month ? dayjs(budgetItemPeriod.month) : undefined;
      });
    }
    return res;
  }
}
