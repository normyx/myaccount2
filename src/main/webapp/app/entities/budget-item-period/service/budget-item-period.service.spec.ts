import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IBudgetItemPeriod, BudgetItemPeriod } from '../budget-item-period.model';

import { BudgetItemPeriodService } from './budget-item-period.service';

describe('BudgetItemPeriod Service', () => {
  let service: BudgetItemPeriodService;
  let httpMock: HttpTestingController;
  let elemDefault: IBudgetItemPeriod;
  let expectedResult: IBudgetItemPeriod | IBudgetItemPeriod[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(BudgetItemPeriodService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      date: currentDate,
      month: currentDate,
      amount: 0,
      isSmoothed: false,
      isRecurrent: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_FORMAT),
          month: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a BudgetItemPeriod', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_FORMAT),
          month: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          month: currentDate,
        },
        returnedFromService
      );

      service.create(new BudgetItemPeriod()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a BudgetItemPeriod', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          date: currentDate.format(DATE_FORMAT),
          month: currentDate.format(DATE_FORMAT),
          amount: 1,
          isSmoothed: true,
          isRecurrent: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          month: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a BudgetItemPeriod', () => {
      const patchObject = Object.assign(
        {
          amount: 1,
          isSmoothed: true,
          isRecurrent: true,
        },
        new BudgetItemPeriod()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
          month: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of BudgetItemPeriod', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          date: currentDate.format(DATE_FORMAT),
          month: currentDate.format(DATE_FORMAT),
          amount: 1,
          isSmoothed: true,
          isRecurrent: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          month: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a BudgetItemPeriod', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addBudgetItemPeriodToCollectionIfMissing', () => {
      it('should add a BudgetItemPeriod to an empty array', () => {
        const budgetItemPeriod: IBudgetItemPeriod = { id: 123 };
        expectedResult = service.addBudgetItemPeriodToCollectionIfMissing([], budgetItemPeriod);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(budgetItemPeriod);
      });

      it('should not add a BudgetItemPeriod to an array that contains it', () => {
        const budgetItemPeriod: IBudgetItemPeriod = { id: 123 };
        const budgetItemPeriodCollection: IBudgetItemPeriod[] = [
          {
            ...budgetItemPeriod,
          },
          { id: 456 },
        ];
        expectedResult = service.addBudgetItemPeriodToCollectionIfMissing(budgetItemPeriodCollection, budgetItemPeriod);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a BudgetItemPeriod to an array that doesn't contain it", () => {
        const budgetItemPeriod: IBudgetItemPeriod = { id: 123 };
        const budgetItemPeriodCollection: IBudgetItemPeriod[] = [{ id: 456 }];
        expectedResult = service.addBudgetItemPeriodToCollectionIfMissing(budgetItemPeriodCollection, budgetItemPeriod);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(budgetItemPeriod);
      });

      it('should add only unique BudgetItemPeriod to an array', () => {
        const budgetItemPeriodArray: IBudgetItemPeriod[] = [{ id: 123 }, { id: 456 }, { id: 28174 }];
        const budgetItemPeriodCollection: IBudgetItemPeriod[] = [{ id: 123 }];
        expectedResult = service.addBudgetItemPeriodToCollectionIfMissing(budgetItemPeriodCollection, ...budgetItemPeriodArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const budgetItemPeriod: IBudgetItemPeriod = { id: 123 };
        const budgetItemPeriod2: IBudgetItemPeriod = { id: 456 };
        expectedResult = service.addBudgetItemPeriodToCollectionIfMissing([], budgetItemPeriod, budgetItemPeriod2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(budgetItemPeriod);
        expect(expectedResult).toContain(budgetItemPeriod2);
      });

      it('should accept null and undefined values', () => {
        const budgetItemPeriod: IBudgetItemPeriod = { id: 123 };
        expectedResult = service.addBudgetItemPeriodToCollectionIfMissing([], null, budgetItemPeriod, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(budgetItemPeriod);
      });

      it('should return initial array if no BudgetItemPeriod is added', () => {
        const budgetItemPeriodCollection: IBudgetItemPeriod[] = [{ id: 123 }];
        expectedResult = service.addBudgetItemPeriodToCollectionIfMissing(budgetItemPeriodCollection, undefined, null);
        expect(expectedResult).toEqual(budgetItemPeriodCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
