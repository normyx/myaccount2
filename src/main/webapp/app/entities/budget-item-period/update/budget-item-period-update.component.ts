import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IBudgetItemPeriod, BudgetItemPeriod } from '../budget-item-period.model';
import { BudgetItemPeriodService } from '../service/budget-item-period.service';
import { IOperation } from 'app/entities/operation/operation.model';
import { OperationService } from 'app/entities/operation/service/operation.service';
import { IBudgetItem } from 'app/entities/budget-item/budget-item.model';
import { BudgetItemService } from 'app/entities/budget-item/service/budget-item.service';

@Component({
  selector: 'jhi-budget-item-period-update',
  templateUrl: './budget-item-period-update.component.html',
})
export class BudgetItemPeriodUpdateComponent implements OnInit {
  isSaving = false;

  operationsCollection: IOperation[] = [];
  budgetItemsSharedCollection: IBudgetItem[] = [];

  editForm = this.fb.group({
    id: [],
    date: [],
    month: [null, [Validators.required]],
    amount: [null, [Validators.required]],
    isSmoothed: [],
    isRecurrent: [],
    operation: [],
    budgetItem: [],
  });

  constructor(
    protected budgetItemPeriodService: BudgetItemPeriodService,
    protected operationService: OperationService,
    protected budgetItemService: BudgetItemService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ budgetItemPeriod }) => {
      this.updateForm(budgetItemPeriod);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const budgetItemPeriod = this.createFromForm();
    if (budgetItemPeriod.id !== undefined) {
      this.subscribeToSaveResponse(this.budgetItemPeriodService.update(budgetItemPeriod));
    } else {
      this.subscribeToSaveResponse(this.budgetItemPeriodService.create(budgetItemPeriod));
    }
  }

  trackOperationById(index: number, item: IOperation): number {
    return item.id!;
  }

  trackBudgetItemById(index: number, item: IBudgetItem): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBudgetItemPeriod>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(budgetItemPeriod: IBudgetItemPeriod): void {
    this.editForm.patchValue({
      id: budgetItemPeriod.id,
      date: budgetItemPeriod.date,
      month: budgetItemPeriod.month,
      amount: budgetItemPeriod.amount,
      isSmoothed: budgetItemPeriod.isSmoothed,
      isRecurrent: budgetItemPeriod.isRecurrent,
      operation: budgetItemPeriod.operation,
      budgetItem: budgetItemPeriod.budgetItem,
    });

    this.operationsCollection = this.operationService.addOperationToCollectionIfMissing(
      this.operationsCollection,
      budgetItemPeriod.operation
    );
    this.budgetItemsSharedCollection = this.budgetItemService.addBudgetItemToCollectionIfMissing(
      this.budgetItemsSharedCollection,
      budgetItemPeriod.budgetItem
    );
  }

  protected loadRelationshipsOptions(): void {
    this.operationService
      .query({ 'budgetItemId.specified': 'false' })
      .pipe(map((res: HttpResponse<IOperation[]>) => res.body ?? []))
      .pipe(
        map((operations: IOperation[]) =>
          this.operationService.addOperationToCollectionIfMissing(operations, this.editForm.get('operation')!.value)
        )
      )
      .subscribe((operations: IOperation[]) => (this.operationsCollection = operations));

    this.budgetItemService
      .query()
      .pipe(map((res: HttpResponse<IBudgetItem[]>) => res.body ?? []))
      .pipe(
        map((budgetItems: IBudgetItem[]) =>
          this.budgetItemService.addBudgetItemToCollectionIfMissing(budgetItems, this.editForm.get('budgetItem')!.value)
        )
      )
      .subscribe((budgetItems: IBudgetItem[]) => (this.budgetItemsSharedCollection = budgetItems));
  }

  protected createFromForm(): IBudgetItemPeriod {
    return {
      ...new BudgetItemPeriod(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value,
      month: this.editForm.get(['month'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      isSmoothed: this.editForm.get(['isSmoothed'])!.value,
      isRecurrent: this.editForm.get(['isRecurrent'])!.value,
      operation: this.editForm.get(['operation'])!.value,
      budgetItem: this.editForm.get(['budgetItem'])!.value,
    };
  }
}
