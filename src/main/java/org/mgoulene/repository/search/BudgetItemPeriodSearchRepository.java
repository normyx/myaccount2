package org.mgoulene.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.List;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.elasticsearch.search.sort.SortBuilder;
import org.mgoulene.domain.BudgetItemPeriod;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link BudgetItemPeriod} entity.
 */
public interface BudgetItemPeriodSearchRepository
    extends ElasticsearchRepository<BudgetItemPeriod, Long>, BudgetItemPeriodSearchRepositoryInternal {}

interface BudgetItemPeriodSearchRepositoryInternal {
    Page<BudgetItemPeriod> search(String query, Pageable pageable);
}

class BudgetItemPeriodSearchRepositoryInternalImpl implements BudgetItemPeriodSearchRepositoryInternal {

    private final ElasticsearchRestTemplate elasticsearchTemplate;

    BudgetItemPeriodSearchRepositoryInternalImpl(ElasticsearchRestTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public Page<BudgetItemPeriod> search(String query, Pageable pageable) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        nativeSearchQuery.setPageable(pageable);
        List<BudgetItemPeriod> hits = elasticsearchTemplate
            .search(nativeSearchQuery, BudgetItemPeriod.class)
            .map(SearchHit::getContent)
            .stream()
            .collect(Collectors.toList());

        return new PageImpl<>(hits, pageable, hits.size());
    }
}
