package org.mgoulene.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.stream.Stream;
import org.mgoulene.domain.SubCategory;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link SubCategory} entity.
 */
public interface SubCategorySearchRepository extends ElasticsearchRepository<SubCategory, Long>, SubCategorySearchRepositoryInternal {}

interface SubCategorySearchRepositoryInternal {
    Stream<SubCategory> search(String query);
}

class SubCategorySearchRepositoryInternalImpl implements SubCategorySearchRepositoryInternal {

    private final ElasticsearchRestTemplate elasticsearchTemplate;

    SubCategorySearchRepositoryInternalImpl(ElasticsearchRestTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public Stream<SubCategory> search(String query) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        return elasticsearchTemplate.search(nativeSearchQuery, SubCategory.class).map(SearchHit::getContent).stream();
    }
}
