package org.mgoulene.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.mgoulene.domain.BudgetItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the BudgetItem entity.
 */
@Repository
public interface BudgetItemRepository
    extends JpaRepository<BudgetItem, Long>, JpaSpecificationExecutor<BudgetItem>, MyaBudgetItemRepository {
    @Query("select budgetItem from BudgetItem budgetItem where budgetItem.account.login = ?#{principal.username}")
    List<BudgetItem> findByAccountIsCurrentUser();

    default Optional<BudgetItem> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<BudgetItem> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<BudgetItem> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct budgetItem from BudgetItem budgetItem left join fetch budgetItem.category left join fetch budgetItem.account",
        countQuery = "select count(distinct budgetItem) from BudgetItem budgetItem"
    )
    Page<BudgetItem> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct budgetItem from BudgetItem budgetItem left join fetch budgetItem.category left join fetch budgetItem.account")
    List<BudgetItem> findAllWithToOneRelationships();

    @Query(
        "select budgetItem from BudgetItem budgetItem left join fetch budgetItem.category left join fetch budgetItem.account where budgetItem.id =:id"
    )
    Optional<BudgetItem> findOneWithToOneRelationships(@Param("id") Long id);

    @Query(
        "SELECT bi FROM BudgetItem as bi inner join bi.budgetItemPeriods as bip WHERE bi.account.login = ?#{principal.username} AND bip.month <= :monthTo AND bip.month >= :monthFrom GROUP BY bi HAVING count(bip) >0 ORDER BY bi.order ASC"
    )
    List<BudgetItem> findAllAvailableInPeriod(@Param("monthFrom") LocalDate monthFrom, @Param("monthTo") LocalDate monthTo);

    @Query(
        "SELECT bi FROM BudgetItem as bi WHERE bi.account.id = :accountId AND bi.order = (SELECT MAX(bi2.order) FROM BudgetItem bi2 WHERE bi2.order < :order AND bi2.account.id = :accountId)"
    )
    List<BudgetItem> findPreviousOrderBudgetItem(@Param("accountId") Long accountId, @Param("order") Integer order);

    @Query(
        "SELECT bi FROM BudgetItem as bi WHERE bi.account.id = :accountId AND bi.order = (SELECT MIN(bi2.order) FROM BudgetItem bi2 WHERE bi2.order > :order AND bi2.account.id = :accountId)"
    )
    List<BudgetItem> findNextOrderBudgetItem(@Param("accountId") Long accountId, @Param("order") Integer order);

    @Query("SELECT MAX(bi.order) + 1 FROM BudgetItem as bi WHERE bi.account.id = :accountId")
    Integer findNewOrder(@Param("accountId") Long accountId);
}
