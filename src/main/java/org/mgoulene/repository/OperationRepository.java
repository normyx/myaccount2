package org.mgoulene.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.mgoulene.domain.Operation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Operation entity.
 */
@Repository
public interface OperationRepository extends JpaRepository<Operation, Long>, JpaSpecificationExecutor<Operation> {
    @Query("select operation from Operation operation where operation.account.login = ?#{principal.username}")
    List<Operation> findByAccountIsCurrentUser();

    default Optional<Operation> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Operation> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Operation> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct operation from Operation operation left join fetch operation.subCategory left join fetch operation.account",
        countQuery = "select count(distinct operation) from Operation operation"
    )
    Page<Operation> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct operation from Operation operation left join fetch operation.subCategory left join fetch operation.account")
    List<Operation> findAllWithToOneRelationships();

    @Query(
        "select operation from Operation operation left join fetch operation.subCategory left join fetch operation.account where operation.id =:id"
    )
    Optional<Operation> findOneWithToOneRelationships(@Param("id") Long id);

    @Query(
        "SELECT operation FROM Operation operation where operation.date = :date AND FLOOR(operation.amount) = FLOOR(:amount) AND operation.label = :label AND operation.account.id = :accountId AND operation.isUpToDate = false"
    )
    List<Operation> findAllByDateAmountLabelAccountAndNotUpToDate(
        @Param("date") LocalDate date,
        @Param("amount") float amount,
        @Param("label") String label,
        @Param("accountId") Long accountId
    );

    @Query("SELECT operation FROM Operation operation where operation.account.id = :accountId")
    List<Operation> findAllByAccount(@Param("accountId") Long accountId);

    @Modifying
    @Query("UPDATE Operation operation SET operation.isUpToDate = false WHERE operation.account.id = :accountId")
    int updateIsUpToDate(@Param("accountId") Long accountId);

    @Modifying
    @Query("DELETE FROM Operation operation WHERE operation.account.id = :accountId AND operation.isUpToDate = false")
    int deleteIsNotUpToDate(@Param("accountId") Long accountId);

    @Query(
        "SELECT operation FROM Operation operation WHERE operation.account.id = :accountId AND operation.subCategory.category.id = :categoryId AND ABS((operation.amount - :value)/operation.amount) < 0.2 AND operation.date > :dateFrom AND operation.date < :dateTo "
    )
    List<Operation> findAllCloseToBudgetItemPeriod(
        @Param("accountId") Long accountId,
        @Param("categoryId") Long categoryId,
        @Param("value") float value,
        @Param("dateFrom") LocalDate dateFrom,
        @Param("dateTo") LocalDate dateTo
    );

    @Query(
        "SELECT operation FROM Operation operation WHERE operation.account.id = :accountId AND operation.subCategory.category.id = :categoryId AND ABS((operation.amount - :value)/operation.amount) < 0.2 AND operation.date > :dateFrom AND operation.date < :dateTo AND NOT operation IN (SELECT bip.operation FROM BudgetItemPeriod bip WHERE bip.operation.id IS NOT NULL )"
    )
    List<Operation> findAllCloseToBudgetItemPeriodWithoutAlreadyAssigned(
        @Param("accountId") Long accountId,
        @Param("categoryId") Long categoryId,
        @Param("value") float value,
        @Param("dateFrom") LocalDate dateFrom,
        @Param("dateTo") LocalDate dateTo
    );

    @Query("SELECT MAX(operation.date) FROM Operation operation where operation.account.id = :accountId")
    LocalDate findLastOperationDate(@Param("accountId") Long accountId);
}
