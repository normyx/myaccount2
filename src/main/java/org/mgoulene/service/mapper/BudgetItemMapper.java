package org.mgoulene.service.mapper;

import org.mapstruct.*;
import org.mgoulene.domain.BudgetItem;
import org.mgoulene.service.dto.BudgetItemDTO;

/**
 * Mapper for the entity {@link BudgetItem} and its DTO {@link BudgetItemDTO}.
 */
@Mapper(componentModel = "spring", uses = { CategoryMapper.class, UserMapper.class })
public interface BudgetItemMapper extends EntityMapper<BudgetItemDTO, BudgetItem> {
    @Mapping(target = "category", source = "category", qualifiedByName = "categoryName")
    @Mapping(target = "account", source = "account", qualifiedByName = "login")
    BudgetItemDTO toDto(BudgetItem s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    BudgetItemDTO toDtoId(BudgetItem budgetItem);
}
