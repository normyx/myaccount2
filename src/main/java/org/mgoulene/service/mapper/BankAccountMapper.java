package org.mgoulene.service.mapper;

import org.mapstruct.*;
import org.mgoulene.domain.BankAccount;
import org.mgoulene.service.dto.BankAccountDTO;

/**
 * Mapper for the entity {@link BankAccount} and its DTO {@link BankAccountDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface BankAccountMapper extends EntityMapper<BankAccountDTO, BankAccount> {
    @Mapping(target = "account", source = "account", qualifiedByName = "login")
    BankAccountDTO toDto(BankAccount s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    BankAccountDTO toDtoId(BankAccount bankAccount);
}
