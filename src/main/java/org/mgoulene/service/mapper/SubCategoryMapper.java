package org.mgoulene.service.mapper;

import org.mapstruct.*;
import org.mgoulene.domain.SubCategory;
import org.mgoulene.service.dto.SubCategoryDTO;

/**
 * Mapper for the entity {@link SubCategory} and its DTO {@link SubCategoryDTO}.
 */
@Mapper(componentModel = "spring", uses = { CategoryMapper.class })
public interface SubCategoryMapper extends EntityMapper<SubCategoryDTO, SubCategory> {
    @Mapping(target = "category", source = "category", qualifiedByName = "categoryName")
    SubCategoryDTO toDto(SubCategory s);

    @Named("subCategoryName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "subCategoryName", source = "subCategoryName")
    SubCategoryDTO toDtoSubCategoryName(SubCategory subCategory);
}
