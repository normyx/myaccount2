package org.mgoulene.service.mapper;

import org.mapstruct.*;
import org.mgoulene.domain.BudgetItemPeriod;
import org.mgoulene.service.dto.BudgetItemPeriodDTO;

/**
 * Mapper for the entity {@link BudgetItemPeriod} and its DTO {@link BudgetItemPeriodDTO}.
 */
@Mapper(componentModel = "spring", uses = { OperationMapper.class, BudgetItemMapper.class })
public interface BudgetItemPeriodMapper extends EntityMapper<BudgetItemPeriodDTO, BudgetItemPeriod> {
    @Mapping(target = "operation", source = "operation", qualifiedByName = "id")
    @Mapping(target = "budgetItem", source = "budgetItem", qualifiedByName = "id")
    BudgetItemPeriodDTO toDto(BudgetItemPeriod s);
}
