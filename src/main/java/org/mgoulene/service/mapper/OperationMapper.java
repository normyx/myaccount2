package org.mgoulene.service.mapper;

import org.mapstruct.*;
import org.mgoulene.domain.Operation;
import org.mgoulene.service.dto.OperationDTO;

/**
 * Mapper for the entity {@link Operation} and its DTO {@link OperationDTO}.
 */
@Mapper(componentModel = "spring", uses = { SubCategoryMapper.class, UserMapper.class, BankAccountMapper.class })
public interface OperationMapper extends EntityMapper<OperationDTO, Operation> {
    @Mapping(target = "subCategory", source = "subCategory", qualifiedByName = "subCategoryName")
    @Mapping(target = "account", source = "account", qualifiedByName = "login")
    @Mapping(target = "bankAccount", source = "bankAccount", qualifiedByName = "id")
    OperationDTO toDto(Operation s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OperationDTO toDtoId(Operation operation);
}
